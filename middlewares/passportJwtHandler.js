const passport = require("passport");

const authenticateJwt = (req, res, next) => {
  passport.authenticate("jwt", { session: false }, function (err, user) {
    if (err) return next(err);
    if (!user) throw { code: 401, message: "Unauthorized" };
    req.user = user;
    next();
  })(req, res, next);
};

module.exports = authenticateJwt;
