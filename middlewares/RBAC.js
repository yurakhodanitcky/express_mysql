const { get } = require("lodash");

const RBAC = (roles) => (req, res, next) => {
  const userRole = get(req, ["user", "role"]);
  if (roles.includes(userRole)) {
    next();
  } else {
    res.status(403).send({
      message: "Permission required to access this resource",
    });
  }
};

module.exports = RBAC;
