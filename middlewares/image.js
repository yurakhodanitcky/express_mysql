const db = require("../models");
const User = db.users;
const path = require("path");
const _ = require("lodash");
const AWS = require("aws-sdk");
const config = require("config");
const { v4 } = require("uuid");
const uploadUserPhoto = async (req, res, next) => {
  try {
    const fileObject = _.get(req, ["files", "0"]);
    const userId = _.get(req, ["user", "id"], null);
    const s3 = new AWS.S3();
    const name = v4() + path.extname(fileObject.originalname);
    const uploadParams = {
      Bucket: config.get("s3.bucket"),
      Key: name,
      Body: Buffer.from(fileObject.buffer, "base64"),
      ACL: "public-read",
    };
    s3.upload(uploadParams, (err, data) => {
      if (err) {
        console.log(err.message);
        throw { code: 400, message: "An error occured while uploading photo" };
      }
      User.findByPk(userId).then(function (user) {
        user.update({ photo_url: data.Location }).then(() => {
          res.status(200).send();
        });
      });
    });
  } catch (e) {
    res.status(_.get(e, "code", 500)).send(_.get(e, "message", e));
  }
};

module.exports = {
  uploadUserPhoto,
};
