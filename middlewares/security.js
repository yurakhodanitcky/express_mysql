const db = require("../models");
const User = db.users;
const Op = db.Sequelize.Op;
const { hash } = require("../utils/bcrypt");
const activationKey = require("./../utils/random");
const { sendActivationEmail, sendEmailWithReset } = require("./../utils/email");
const passport = require("passport");
const _ = require("lodash");
const axios = require("axios");
const baseUrl = require("./../utils/url");

const registerUser = async (req, res) => {
  try {
    let user = req.body;
    user.password = hash(user.password);
    user.activation_key = activationKey();

    const count = await User.count({ where: { email: user.email } });
    if (count > 0) {
      res.status(400).json({ message: "Email is already in use" });
    } else {
      const result = await User.create(user);
      const url =
        baseUrl(req) + "/auth/confirm-registration/" + user.activation_key;
      sendActivationEmail(url, result.email);
      res.status(201).json({ id: result.id });
    }
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const activateUser = async (req, res) => {
  try {
    let user = await User.findOne({
      where: {
        activation_key: req.params.key,
      },
    });
    if (user) {
      await user.update({
        activation_key: null,
        is_activated: true,
      });
      res.status(200).json({ message: "User activated" });
    } else {
      res.status(404).json({ message: "Activation key not found" });
    }
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const signInUser = async (req, res, next) => {
  try {
    return passport.authenticate(
      "local",
      {
        session: false,
      },
      (err, user, info) => {
        if (err) {
          return res.status(400).json(err);
        }
        if (user) {
          const token = user.generateJWT();
          res.status(200).json({ token });
        } else {
          res.status(400).send(info);
        }
      }
    )(req, res, next);
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const facebookAuth = async (req, res, next) => {
  try {
    const accessToken = _.get(req, "body.access_token");
    const { data } = await axios({
      url: "https://graph.facebook.com/me",
      method: "get",
      params: {
        fields: ["id", "email", "first_name", "last_name"].join(","),
        access_token: accessToken,
      },
    });

    let user = await User.findOne({
      attributes: ["id", "email", "role", "facebook_id"],
      where: {
        [Op.or]: [
          {
            email: data.email,
          },
          {
            facebook_id: data.id,
          },
        ],
      },
    });

    if (user) {
      if (!user.facebook_id) {
        await user.update({
          facebook_id: data.id,
        });
      }
      const token = user.generateJWT();
      res.status(200).send({ token });
    } else {
      user = {
        email: data.email,
        first_name: data.first_name,
        last_name: data.last_name,
        facebook_id: data.id,
        is_activated: true,
      };
      user = await User.create(user);
      const token = user.generateJWT();
      res.status(200).send({ token });
    }
  } catch (e) {
    res
      .status(_.get(e, "status", 401))
      .send({ message: _.get(e, "message", e) });
  }
};

const googleAuth = async (req, res, next) => {
  try {
    const accessToken = _.get(req, "body.access_token");
    const { data } = await axios({
      url: "https://www.googleapis.com/oauth2/v2/userinfo",
      method: "get",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    let user = await User.findOne({
      attributes: ["id", "email", "role", "google_id"],
      where: {
        [Op.or]: [{ email: data.email }, { google_id: data.id }],
      },
    });

    if (user) {
      if (!user.google_id) {
        await user.update({ google_id: data.id });
      }
      const token = user.generateJWT();
      res.status(200).send({ token });
    } else {
      user = {
        email: data.email,
        first_name: data.given_name,
        last_name: data.family_name,
        google_id: data.id,
        is_activated: true,
      };
      user = await User.create(user);
      const token = user.generateJWT();
      res.status(200).send({ token });
    }
  } catch (e) {
    res.status(_.get(e, "status", 401)).send({
      message: _.get(e, "message", e),
    });
  }
};

const resetUserCredentials = async (req, res, next) => {
  try {
    const body = _.get(req, "body");
    const user = await User.findOne({ where: { email: body.email } });
    if (user) {
      const key = activationKey();
      await user.update({ activation_key: key });
      const url = baseUrl(req) + "/auth/reset-finish/" + user.activation_key;
      sendEmailWithReset(url, user.email);
    }
    res.status(200).json({ message: "Link was sent to email" });
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const resetFinish = async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { activation_key: req.params.key },
    });
    if (!user) {
      return res.status(400).send({
        message: "User not registered or restore link out of date",
      });
    } else {
      const password = _.get(req.body, "password");
      await user.update({ password: hash(password), activation_key: null });
      res.status(200).send();
    }
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

module.exports = {
  registerUser,
  activateUser,
  signInUser,
  facebookAuth,
  googleAuth,
  resetUserCredentials,
  resetFinish,
};
