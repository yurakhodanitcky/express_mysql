const db = require("../models");
const User = db.users;
const _ = require("lodash");

const findAllUsers = async (req, res) => {
  try {
    const size = Number(_.get(req, "query.size", 20));
    const page = Number(_.get(req, "query.page", 0));

    const users = await User.findAndCountAll({
      offset: page * size,
      limit: size,
      where: {},
      attributes: [
        "id",
        "first_name",
        "last_name",
        "photo_url",
        "created",
        "updated",
      ],
    });
    const result = {
      content: users.rows,
      totalElements: users.count,
      totalPages: Math.ceil(users.count / size),
      size: size,
      page: page,
      numberOfElements: users.rows.length,
    };
    res.status(200).json(result);
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const findUserById = async (req, res) => {
  try {
    User.findByPk(req.params.id).then(function (user) {
      if (user) res.json(user);
      else res.status(404).json({ message: "User not found" });
    });
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};

const deleteUserById = async (req, res) => {
  try {
    const result = await User.destroy({ where: { id: req.params.id } });
    if (result === 0) res.status(404).json({ message: "User not found" });
    else res.status(200).send();
  } catch (e) {
    res.status(_.get(e, "status", 500)).send({
      message: _.get(e, "message", e),
    });
  }
};

const updateUser = async (req, res) => {
  try {
    const userId = _.get(req, ["user", "id"], null);
    User.findByPk(userId).then(function (user) {
      user
        .update({
          first_name: req.body.first_name,
          last_name: req.body.last_name,
        })
        .then(() => res.status(200).send());
    });
  } catch (e) {
    res.status(400).send({ message: _.get(e, "message", e) });
  }
};
module.exports = {
  findAllUsers,
  findUserById,
  deleteUserById,
  updateUser,
};
