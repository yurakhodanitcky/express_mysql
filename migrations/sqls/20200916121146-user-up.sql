CREATE TABLE `users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `google_id` VARCHAR(100) DEFAULT NULL,
  `facebook_id` VARCHAR(100) DEFAULT NULL,
  `email` VARCHAR(100) DEFAULT NULL,
  `activation_key` VARCHAR(20) DEFAULT NULL,
  `is_activated` BOOLEAN DEFAULT false,
  `password` VARCHAR(60) DEFAULT NULL,
  `first_name` VARCHAR(80) NOT NULL,
  `last_name` VARCHAR(80) NOT NULL,
  `role` enum ('ROLE_ADMIN', 'ROLE_USER') NOT NULL DEFAULT 'ROLE_USER',
  `photo_url` VARCHAR(255) DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email` (`email`),
  UNIQUE INDEX `facebook_id` (`facebook_id`),
  UNIQUE INDEX `google_id` (`google_id`),
  CONSTRAINT `social_check_null` CHECK (
    COALESCE(`google_id`, `facebook_id`, `email`) IS NOT NULL
  )
) ENGINE = InnoDB;

INSERT INTO users (id, email, is_activated, password, first_name, last_name, role) VALUES
(1, 'admin@gmail.com', 1, '$2b$12$RGtep8ztHf7I55nFZSNMZeRmt7bcfodIYk50BNBEKyrM/xTTrh9w6', 'Admin', 'Admin', 'ROLE_ADMIN');