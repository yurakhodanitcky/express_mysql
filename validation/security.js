const JoiBase = require("@hapi/joi");
const JoiDate = require("@hapi/joi-date");
const Joi = JoiBase.extend(JoiDate);

const emailValidator = Joi.string()
  .email()
  .max(100)
  .required()
  .error(() => new Error("The email field must be a valid email"));
const passwordRegex = new RegExp(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/);
const passwordValidation = Joi.string()
  .regex(passwordRegex)
  .required()
  .error(
    () =>
      new Error(
        "The password must contain at least one digit, one lowercase and one uppercase letter and be more than 7 symbols"
      )
  );

const registrationSchema = Joi.object({
  email: emailValidator,
  password: passwordValidation,
  first_name: Joi.string().max(80).required(),
  last_name: Joi.string().max(80).required(),
});

const updateSchema = Joi.object({
  first_name: Joi.string().max(80).required(),
  last_name: Joi.string().max(80).required(),
});

const signInSchema = Joi.object({
  email: emailValidator,
  password: Joi.string().required(),
});

const restoreSchema = Joi.object({
  email: emailValidator,
});

const passwordSchema = Joi.object({
  password: passwordValidation,
});

const socialNetworkAuth = Joi.object({
  access_token: Joi.string().required(),
});

module.exports = {
  registrationSchema,
  signInSchema,
  restoreSchema,
  socialNetworkAuth,
  passwordSchema,
  updateSchema,
};
