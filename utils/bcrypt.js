const bcrypt = require("bcrypt");

const hash = (data, saltOrRounds = 12) => bcrypt.hashSync(data, saltOrRounds);
const compare = (data, hashedData) => bcrypt.compareSync(data, hashedData);

module.exports = {
  hash,
  compare,
};
