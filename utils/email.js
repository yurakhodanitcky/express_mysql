const nodemailer = require("nodemailer");
const config = require("config");
const sender = config.get("mail.username");
const transporter = nodemailer.createTransport({
  host: config.get("mail.host"),
  port: config.get("mail.port"),
  secure: false,
  auth: {
    user: sender,
    pass: config.get("mail.password"),
  },
});

const sendActivationEmail = async (url, email) => {
  try {
    return await transporter.sendMail({
      from: sender,
      to: email,
      subject: "Confirm registration",
      html: `This message was sent automatically do not response. <p/> <i>To confirm your email, please follow the <a href="${url}">link</a></i>`,
    });
  } catch (e) {
    throw e;
  }
};

const sendEmailWithReset = async (url, email) => {
  return await transporter.sendMail({
    from: sender,
    to: email,
    subject: "Confirm reset",
    html: `This message was sent automatically do not response. <p/> <i>To confirm your request to reset password, please follow the <a href="${url}">link</a></i>`,
  });
};

module.exports = {
  sendActivationEmail,
  sendEmailWithReset,
};
