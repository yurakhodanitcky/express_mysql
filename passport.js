const passport = require("passport");
const LocalStrategy = require("passport-local");
const passportJWT = require("passport-jwt");
const config = require("config");
const ExtractJWT = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;
const db = require("./models");
const User = db.users;
const salt = config.get("service.salt");

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
    },
    async (email, password, done) => {
      const user = await User.findOne({
        where: { email: email },
        attributes: ["id", "password", "email", "role", "is_activated"],
      });
      if (!user || !user.validPassword(password)) {
        return done(null, false, { message: "Incorrect email or password" });
      }
      if (!user.is_activated) {
        return done(null, false, { message: "Please confirm you email" });
      }
      return done(null, user);
    }
  )
);

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: salt,
    },
    async (jwtPayload, done) => {
      const user = await User.findOne({
        where: { id: jwtPayload.id },
        attributes: ["id", "password", "role"],
      });
      return done(null, user);
    }
  )
);
