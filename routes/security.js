const express = require("express");
const router = express.Router();
const {
  registerUser,
  activateUser,
  signInUser,
  facebookAuth,
  googleAuth,
  resetUserCredentials,
  resetFinish,
} = require("../middlewares/security");
const validator = require("express-joi-validation").createValidator({
  passError: true,
});
const {
  registrationSchema,
  signInSchema,
  restoreSchema,
  socialNetworkAuth,
  passwordSchema,
} = require("../validation/security");

router.post("/register", validator.body(registrationSchema), registerUser);
router.get("/confirm-registration/:key", activateUser);
router.post("/sign-in", validator.body(signInSchema), signInUser);
router.post("/google", validator.body(socialNetworkAuth), googleAuth);
router.post("/facebook", validator.body(socialNetworkAuth), facebookAuth);
router.post("/reset", validator.body(restoreSchema), resetUserCredentials);
router.post("/reset-finish/:key", validator.body(passwordSchema), resetFinish);
module.exports = router;
