const multer = require("multer");
const express = require("express");
const router = express.Router();
const config = require("config");
const passportJwtHandler = require("../middlewares/passportJwtHandler");
const RBAC = require("../middlewares/RBAC");
const ADMIN = config.get("service.roles.admin");
const upload = multer().any();
const { uploadUserPhoto } = require("../middlewares/image");
const validator = require("express-joi-validation").createValidator({
  passError: true,
});
const { updateSchema } = require("../validation/security");
const {
  findAllUsers,
  findUserById,
  deleteUserById,
  updateUser,
} = require("../middlewares/users");

router.get("/", [passportJwtHandler, RBAC([ADMIN])], findAllUsers);
router.get("/:id", [passportJwtHandler, RBAC([ADMIN])], findUserById);
router.delete("/:id", [passportJwtHandler, RBAC([ADMIN])], deleteUserById);
router.put("/", validator.body(updateSchema), passportJwtHandler, updateUser);
router.post(
  "/photo",
  passportJwtHandler,
  function (req, res, next) {
    upload(req, res, function (err) {
      if (err || err instanceof multer.MulterError) {
        res.status(500).send({ message: err });
      } else {
        next();
      }
    });
  },
  [uploadUserPhoto]
);

module.exports = router;
