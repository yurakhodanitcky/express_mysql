const config = require("config");
const jwt = require("jsonwebtoken");
const { compare } = require("../utils/bcrypt");
const salt = config.get("service.salt");

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    google_id: {
      type: Sequelize.STRING(100),
      defaultValue: null,
      unique: true,
      index: true,
    },
    facebook_id: {
      type: Sequelize.STRING(100),
      defaultValue: null,
      unique: true,
      index: true,
    },
    email: {
      type: Sequelize.STRING(100),
      defaultValue: null,
      unique: true,
      index: true,
    },
    activation_key: {
      type: Sequelize.STRING(20),
      defaultValue: null,
    },
    is_activated: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    password: {
      type: Sequelize.STRING(100),
      defaultValue: null,
    },
    first_name: {
      type: Sequelize.STRING(80),
      allowNull: false,
    },
    last_name: {
      type: Sequelize.STRING(80),
      allowNull: false,
    },
    role: {
      type: Sequelize.ENUM("ROLE_ADMIN", "ROLE_USER"),
      defaultValue: "ROLE_USER",
      allowNull: false,
    },
    photo_url: {
      type: Sequelize.STRING(400),
      defaultValue: null,
    },
    createdAt: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      field: "created",
    },
    updatedAt: {
      type: "TIMESTAMP",
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      field: "updated",
    },
  });

  User.prototype.generateJWT = function () {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
    const token = jwt.sign(
      {
        email: this.email,
        id: this.id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
        role: this.role,
      },
      salt
    );
    return `Bearer ${token}`;
  };

  User.prototype.validPassword = function (password) {
    return compare(password, this.password);
  };
  return User;
};
