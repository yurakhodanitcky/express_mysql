const express = require("express");
const path = require("path");
const config = require("config");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const securityRouter = require("./routes/security");
const swaggerUi = require("swagger-ui-express");
const _ = require("lodash");
const swaggerDocument = require("./swagger.json");
const AWS = require('aws-sdk');
require("./passport");

const app = express();

AWS.config.update({
  accessKeyId: config.get('s3.access-key'),
  secretAccessKey: config.get('s3.access-secret'),
  region: config.get('s3.region'),
});

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/auth", securityRouter);
app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use((err, req, res, next) => {
  if (err && err.error && err.type === "body") {
    const error = err.error.toString().split("Error: ")[1];
    res.status(400).json({ message: error });
  } else {
    res
      .status(_.get(err, "code", 500))
      .send({ message: _.get(err, "message", err) });
  }
});

module.exports = app;
